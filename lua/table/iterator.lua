-- SPDX-License-Identifier: AGPL-3.0-or-later
-- Copyright 2023 - Thadeu de Paula and contributors


local t = {
  {'a','10','ok'},
  {'b','20','notok'},
  {'c','30','ok'},
  {'d','40','notok'},
  {'e','50','ok'},
  {'f','60','notok'},
  {'g','70','ok'},
  {'h','80','notok'}
}

local
function gen(val)
  local i=0
  return function()
    i = i+1
    if val[i] then
      return table.unpack(val[i])
    end
    return nil
  end
end


for a, b, c in gen(t) do

  print('A:', a,'B:',b ,'C:',c)
end
