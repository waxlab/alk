-- This is equivalent to the Haskell foldl:
-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl f z []     = z
-- foldl f z (x:xs) = foldl f (f z x) xs

local foldl
function foldl(f, z, xs, i)
  i = i or 1
  if i > #xs then return z end
  return foldl(f, f(z, xs[i]), xs, i+1)
end

-- This is equivalent to Haskell foldr:
-- foldr :: (a -> b -> b) -> b -> [a] -> b
-- foldr f z [] = z
-- foldr f z (x:xs) = f x (foldr f z xs)
local foldr
function foldr(f, z, xs, i)
  i = i or 1
  if i > #xs then return z end
  return f(xs[i], foldr(f,z,xs,i+1))
end



local sum, sub

function sum(a, b) return a + b end
function sub(a, b) return a - b end

local res

res = foldl(sum,0,{1,2,3,4})
assert( res == 10 )

res = foldr(sum,0,{1,2,3,4})
assert( res == 10 )

res = foldl(sub,0,{1,2,3,4})
assert( res == -10 )

res = foldr(sub,0,{1,2,3,4})
assert( res == -2 )
