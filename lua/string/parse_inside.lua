-- SPDX-License-Identifier: AGPL-3.0-or-later
-- Copyright 2023 - Thadeu de Paula and contributors

-- This script shows how parse string enclosed by single and double quotes
-- inside an outer file, useful for parsing documentation or programming
-- language source files. Lines started with ``--|`` are used as example.
-- To use just run ``lua thisfile.lua``.

--| This "is a quote" string "in \"double\" quotes"
--| This 'is a string chunk' inside 'another \'single\' quotes'.
--| This show "exchanged \'quote inside\' its" delimiters.
--| This also 'exchanges \"quote inside\" delimiter' example.

local wax = require 'wax'

local
function extract(txt)
  local res, from, len, fromdelim, delim, a, b = {}, 1, #txt
  while from < len do
    if delim then
      local match = delim =='"' and [[(\*)"]] or [[(\*)']]
      a, b, match = txt:find(match, from)
      if not a then
        return nil, 'missing '..delim..' pair for the one at '..from-1
      end
      if match:len() % 2 == 0 then
        table.insert(res, txt:sub(fromdelim, a-1))
        delim, fromdelim = nil,nil
      end
      from = b+1
    else
      a, b, delim = txt:find([[(['"])]], from)
      if delim then
        table.insert(res, txt:sub(from, a-1))
        from = b+1
        fromdelim = from
      else
        table.insert(res, txt:sub(from))
        return res
      end
    end
  end
  return res
end


local f = io.open(arg[0],'r')
local line = f:read()
local res, text, err = {}
while line do
  text = line:match('^%-%-%|%s*(.*)$')
  if text then
    text, err = extract(text)
    if err then
      table.insert(res, {from=line, error=err})
    else
      for _,v in ipairs(text) do print(v) end
      text.from = line
      table.insert(res, text)
    end
  end
  line = f:read()
end


wax.show(res)


